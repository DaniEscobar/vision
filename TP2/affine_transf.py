import cv2
import numpy as np
import math as ma

ix , iy = -1 , -1
drawing = False      # true if mouse is pressed
pos_x = [-1,-1]
pos_y = [-1,-1]
mode = False
output_pts=[]
point=3

imagen1 = cv2.imread(sys.argv[1])  
imagen2 = cv2.imread(sys.argv[2])
img = imagen1.copy()
img2 = imagen2.copy()
cache = img.copy()

col, row = imagen1.shape[:2]


def affine():
    global img, img2, cache, col, row, out_img, output_pts
    in_pts = np.float32([[0,0], [row-1,0], [0,col-1]])    #esquinas de la imagen
    output_pts = np.float32([output_pts[0], output_pts[1], output_pts[2]])  #Puntos en los que se quiere colocar la segunda imagen
    M = cv2.getAffineTransform(in_pts,output_pts)  #Matriz de transformación

    img2 = cv2.resize(img2,(row,col)) #Para poder pegar una imagen en la otra
    out_img = cv2.warpAffine(img2, M, (row,col))  #Transformo la imagen
    
    #Máscara de la imagen para pegarla
    img2bw = np.zeros([col,row],dtype=np.uint8)
    img2bw.fill(255)
    img2bw = cv2.resize(img2bw,(row,col))
    out_img2 = cv2.warpAffine(img2bw, M, (row,col))
    _, mask = cv2.threshold(out_img2[:,:], 0, 255, cv2.THRESH_BINARY|cv2.THRESH_OTSU)

    #Se copia la imagen donde la máscara sea blanca
    img=imagen1.copy()
    img[np.where(mask == 255)] = out_img[np.where(mask == 255)]
    cache = img.copy()

def draw_circle (event , x , y , flags , param ) :
    global ix , iy , drawing , mode, img, cache, pos_x, pos_y, output_pts, point
    if event == cv2.EVENT_LBUTTONDBLCLK:
        if not mode:
            img = imagen1.copy()
            cache = imagen1.copy()
            drawing = True
            ix , iy = x , y
        else:
            #Con doble click se guardan los 3 puntos 
            if (point > 0):
                output_pts.append([x,y])
                point-=1
                cv2.circle(img, (x,y), 5, (255,0,0), -1)
                cache = img.copy()
                if (point == 0):
                    affine()
    elif event == cv2.EVENT_LBUTTONDOWN:
       if not mode:
            img = imagen1.copy()
            cache = imagen1.copy()
            drawing = True
            ix , iy = x , y
    elif event == cv2.EVENT_LBUTTONUP:
        if not mode:
            drawing = False
            cv2.rectangle(img, (ix,iy), (x,y), (0,0,255), 2)
            cache = img.copy()
            pos_x = [ix,x]
            pos_y = [iy,y]

cv2.namedWindow('image')
cv2.setMouseCallback('image', draw_circle)
while(1):
    cv2.imshow('image', img)
    k = cv2.waitKey(1) & 0xFF
    if k == 27 :
        break
    elif k == ord('r'):
        img = imagen1.copy()
        cache = imagen1.copy()
        pos_x=[-1,-1]
        pos_y=[-1,-1]
        output_pts=[]
        point=3
    elif k == ord('a'):
        out_pts=[]
        point=3
        img = imagen1.copy()
        cache = imagen1.copy()
        mode = not mode
cv2.destroyAllWindows()

