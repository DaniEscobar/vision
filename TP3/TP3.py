'''
    SELECCIÓN DE PUNTOS:
        1)SUP. IZQ.
        2)SUP. DER.
        3)INF. IZQ.
        4)INF. DER.
'''


import cv2
import numpy as np
import math as ma
import sys

mode = False
output_pts=[]
point=4


if(len(sys.argv)>1):
    image=sys.argv[1]
else:
    print('no hay imagen')
    sys.exit(0) 

imagen = cv2.imread(image)  
img = imagen.copy()
cache = img.copy()

col, row = imagen.shape[:2]

def perspective():
    global img, cache, col, row, out_img, output_pts

    px_min= output_pts[0][0]
    px_max= output_pts[3][0]
    py_min= output_pts[0][1]
    py_max= output_pts[3][1]

    px_max-=px_min
    px_min=0
    py_max-=py_min
    py_min=0

    in_pts=np.float32([output_pts[0], output_pts[1], output_pts[2], output_pts[3]])    #puntos de entrada son los seleccionados
    out_pts = np.float32([[px_min,py_min],[px_max,py_min],[px_min,py_max],[px_max,py_max]]) #Los puntos de salida son la esquina superior izquierda y el ancho y alto de la imagen recortada
    M = cv2.getPerspectiveTransform(in_pts,out_pts)  

    img2 = cv2.resize(imagen,(row,col)) 
    out_img = cv2.warpPerspective(img2, M, (px_max,py_max))  #Transformo la imagen
    cv2.imshow('Salida',out_img)


def draw_circle (event , x , y , flags , param ) :
    global ix , iy , mode, img, cache, output_pts, point
    if event == cv2.EVENT_LBUTTONDBLCLK:
        if mode:
            #Con doble click se guardan los 4 puntos 
            if (point > 0):
                output_pts.append([x,y])
                point-=1
                cv2.circle(img, (x,y), 5, (255,0,100), -1)
                if (point==2):
                    cv2.line(img,tuple(output_pts[0]),tuple(output_pts[1]),(255,255,0),1)
                elif(point==1):
                    cv2.line(img,tuple(output_pts[0]),tuple(output_pts[2]),(255,255,0),1)
                elif (point == 0):
                    cv2.line(img,tuple(output_pts[3]),tuple(output_pts[1]),(255,255,0),1)
                    cv2.line(img,tuple(output_pts[2]),tuple(output_pts[3]),(255,255,0),1)
                    perspective()
                cache = img.copy()
    elif event == cv2.EVENT_MOUSEMOVE:
        img = cache.copy()
        cache = img.copy()
        if mode:
            cv2.circle(img, (x,y), 10, (127, 0, 255), -1)
        else:
            cv2.circle(img, (x,y), 8, (100,0,255), -1)


cv2.namedWindow('image')
cv2.setMouseCallback('image', draw_circle)

while(1):
    cv2.imshow('image', img)
    k = cv2.waitKey(1) & 0xFF
    if k == 27 :
        break
    elif k == ord('r'):
        img = imagen.copy()
        cache = imagen.copy()
        output_pts=[]
        point=4
        if(cv2.getWindowProperty('image_out', cv2.WND_PROP_VISIBLE)):
            cv2.destroyWindow('Salida')
    elif k == ord('a'):
        out_pts=[]
        point=4
        img = imagen.copy()
        cache = imagen.copy()
        mode = not mode
cv2.destroyAllWindows()


